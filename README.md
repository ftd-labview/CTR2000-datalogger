# CTR2000 datalogger

A simple LabVIEW datalogger for the ASL CTR2000 Precision Thermometer.



## Software Requirements

LabVIEW 2019 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
